import numpy as np


def stride_mean(arr, stride):
    # successive means of elements of arr of length stride
    extra = arr.shape[0] % stride
    if extra == 0:
        return np.mean(arr.reshape(-1, stride), axis=1)
    else:
        toslice = arr.shape[0] - extra
        first = np.mean(arr[:toslice].reshape(-1, stride), axis=1)
        rest = np.mean(arr[toslice:])
        return np.hstack((first, rest))
